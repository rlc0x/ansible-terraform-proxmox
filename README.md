Ansible-terraform-proxmox
=========

Ansible playbook that generates Terraform plan files and deploys them to a Proxmox server.

This role is meant to be run from an ansible control node. 

After the role is applied the resulting VMs running on the Proxmox server will be managable from the control node. 

WARNING:
Do not use this playbook in production without extensive testing.


Tested With
------------

    - RHEL 8.7
    - Rocky Linux 8.7
    - Proxmox VE 7.3
    - Ansible 2.9
    - Terraform 1.3.9
    - Terraform telemate/proxmox 2.9.11

Requirements
------------

Proxmox-ve server with an [API Token](https://pve.proxmox.com/pve-docs/pve-admin-guide.html#pveum_tokens) generated that allows creating virtual machines.

Playbook Variables
--------------

Available variables are listed below

    _domain: example.com
	pve_node: pve 
    ipa_server: "{{ vault_ipa_server }}"
    ipa_pass: "{{ vault_ipa_pass }}"
    ipa_user: "{{ vault_ipa_user }}"
    ipa_domain: "{{ _domain }}"
	vm_config:
	  memory: 1024
	  cores: 1
	  disk_size: 20G
	  disk_storage: ZPool1
	  username: ansible
	  password: "{{ vault_ansible_password }}"
	  domain_name: "{{ _domain }}"
	  dns: 192.168.1.12
	  image: rocky8.7-ci
	pve_credentials:
	  api_endpoint: "{{ vault_api_endpoint }}"
	  api_token_id: "{{ vault_api_token_id }}"
	  api_secret: "{{ vault_api_secret }}"
	  api_password: "{{ vault_api_password }}"
	nodes:
	  - name: node0
	    comments: "Ansible node0"
	    ip_gateway: 192.168.1.1
	    ip_address: 192.168.1.40
	    ip_cidr: 24
	    dns: 192.168.1.152
	    fqdn: node0.example.com
	    second_disk: False
	    cdrom: True
	    cores: 1
        manage_ipa_client: True
	    state: absent
	  - name: node1
	    comments: "Ansible node1"
	    ip_gateway: 192.168.1.1
	    ip_address: 192.168.1.41
	    ip_cidr: 24
	    dns: 192.168.1.152
	    fqdn: node1.example.com
	    second_disk: True
	    cdrom: False
        manage_ipa_client: True
	    state: absent
	  - name: node2
	    comments: "Ansible node2"
	    ip_gateway: 192.168.1.1
	    ip_address: 192.168.1.42
	    ip_cidr: 24
	    dns: 192.168.1.152
	    fqdn: node2.example.com
	    second_disk: False
	    cdrom: False
        manage_ipa_client: True
	    state: absent

Use
---

```bash
# Deploy the VMs
ansible-playbook deploy-vms.yml --vault-password-file .vault_password

# Remove the VMs
ansible-playbook remove-vms.yml --vault-password-file .vault_password
```


License
-------

MIT

Author Information
------------------

Raymond Cox
